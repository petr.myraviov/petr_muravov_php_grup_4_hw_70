<?php

namespace Tests;

use App\Article;
use App\Comment;
use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected $faker, $user, $article, $comment;

    public function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        $this->user = factory(User::class)->create();
        $this->article = factory(Article::class)->create(['user_id' => $this->user->id]);
        $this->comment = factory(Comment::class)->create(['user_id' => $this->user->id, 'article_id' => $this->article->id]);

    }
}
