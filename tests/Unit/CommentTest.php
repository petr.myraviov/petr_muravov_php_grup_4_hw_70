<?php

namespace Tests\Unit;


use App\Article;
use App\Comment;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessCreateComment()
    {
        Auth::check($this->user);
        $this->actingAs($this->user);
        $data = [
            'user_id' => $this->user->id,
            'body' => $this->faker->text,
            'article_id' => $this->article->id,
        ];
        $response = $this->json('post',route('comments.store', $data));
        $response->assertCreated();
        $this->assertDatabaseHas('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['body'],$resp->data->body);
        $this->assertEquals($data['article_id'],$resp->data->article->id);
        $this->assertEquals($data['user_id'],$resp->data->user->id);
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testArticleValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'article_id' => '',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('comments.store', $data)
        );
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testBodyValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'article_id' =>$this->article->id,
            'body' => '',
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('comments.store', $data)
        );
        $response->assertStatus(422);
        $this->assertDatabaseMissing('comments', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testUserValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'article_id' =>$this->article->id,
            'body' =>  $this->faker->text,
            'user_id' => ''
        ];
        $response = $this->json(
            'post',
            route('comments.store', $data)
        );
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('article_id', (array)$resp->errors));
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testShowComment()
    {
        $this->actingAs($this->user);
        $response = $this->json('get', route('comments.show' , $this->comment));
        $response->assertSeeText($this->comment->body);
        $response->assertSeeText($this->user->id);
        $response->assertSeeText($this->article->id);
        $response->assertStatus(200);
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testSuccessDestroyComment()
    {
        $this->actingAs($this->user);
        $response = $this->json(
            'delete',
            route('comments.destroy', $this->comment)
        );
        $response->assertNoContent();
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testUpdateComment()
    {

        $this->actingAs($this->user);
        $response = $this->json('put',route('comments.update', $this->comment));
        $response->assertStatus(422);
    }
    /**
     * A basic unit test example.
     * @group comments
     * @return void
     */
    public function testIndexComment()
    {
        $this->actingAs($this->user);
        $response = $this->json('get','comments.index');
        foreach ($this->comment as $comment){
            $response->assertSeeText($comment);
        }
    }
}
