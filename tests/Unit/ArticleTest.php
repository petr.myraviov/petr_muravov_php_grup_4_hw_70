<?php

namespace Tests\Unit;

use App\Article;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessCreateArticle()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => $this->faker->word,
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json('post',route('articles.store', $data));
        $response->assertCreated();
        $this->assertDatabaseHas('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertEquals($data['title'],$resp->data->title);
        $this->assertEquals($data['body'],$resp->data->body);
        $this->assertEquals($data['user_id'],$resp->data->user->id);
    }
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testTitleValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => '',
            'body' => $this->faker->text,
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store', $data)
        );
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testBodyValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'title' =>$this->faker->word,
            'body' => '',
            'user_id' => $this->user->id
        ];
        $response = $this->json(
            'post',
            route('articles.store', $data)
        );
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
        $this->assertFalse(array_key_exists('user_id', (array)$resp->errors));
    }
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testUserValidation()
    {
        $this->actingAs($this->user);
        $data = [
            'title' =>$this->faker->word,
            'body' =>  $this->faker->text,
            'user_id' => ''
        ];
        $response = $this->json(
            'post',
            route('articles.store', $data)
        );
        $response->assertStatus(422);
        $this->assertDatabaseMissing('articles', $data);
        $resp = json_decode($response->getContent());
        $this->assertTrue(array_key_exists('message', (array)$resp));
        $this->assertTrue(array_key_exists('errors', (array)$resp));
        $this->assertTrue(array_key_exists('user_id', (array)$resp->errors));
        $this->assertFalse(array_key_exists('body', (array)$resp->errors));
        $this->assertFalse(array_key_exists('title', (array)$resp->errors));
    }
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testShowArticle()
    {
        $this->actingAs($this->user);
        $response = $this->json('get', route('articles.show' , $this->article));
        $response->assertSeeText($this->article->body);
        $response->assertSeeText($this->article->title);
        $response->assertSeeText($this->user->id);
        $response->assertStatus(200);
    }
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testSuccessDestroyArticle()
    {
        $this->actingAs($this->user);
        $article = factory(Article::class)->create([
            'user_id' => $this->user->id
        ]);
        $response = $this->json(
            'delete',
            route('articles.destroy', $article)
        );
        $response->assertNoContent();
    }
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testUpdateArticle()
    {

        $this->actingAs($this->user);
        $response = $this->json('put',route('articles.update', $this->article));
        $response->assertStatus(422);
    }
    /**
     * A basic unit test example.
     * @group articles
     * @return void
     */
    public function testIndexArticle()
    {
        $this->actingAs($this->user);
        $response = $this->json('get', 'articles.index');
        foreach ($this->article as $article) {
            $response->assertSeeText($article);
        }
    }
}
